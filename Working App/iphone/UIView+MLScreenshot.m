

#import "UIView+MLScreenshot.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIView (MLScreenshot)

- (UIImage *)screenshot
{
    CGFloat scale = [[UIScreen mainScreen] scale];
    UIImage *screenshot;
    
    UIGraphicsBeginImageContextWithOptions(self.frame.size, NO, scale);
    {
        if(UIGraphicsGetCurrentContext() == nil)
        {
            NSLog(@"UIGraphicsGetCurrentContext is nil. You may have a UIView (%@) with no really frame (%@)", [self class], NSStringFromCGRect(self.frame));
        }
        else
        {
            [self.layer renderInContext:UIGraphicsGetCurrentContext()];
            
            screenshot = UIGraphicsGetImageFromCurrentImageContext();
        }
    }
    UIGraphicsEndImageContext();
    
    return screenshot;
}

- (BOOL)isMapViewInSubviews:(NSArray *)subviews
{
    for(id view in subviews)
    {
        if([view isKindOfClass:NSClassFromString(@"MKMapView")])
        {
            return YES;
        }
        else
        {
            UIView *subView = view;
            [self isMapViewInSubviews:subView.subviews];
        }
    }
    
    return NO;
}

@end

