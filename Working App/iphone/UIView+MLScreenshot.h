
#import <UIKit/UIKit.h>

@interface UIView (MLScreenshot)

- (UIImage *)screenshot;

@end
