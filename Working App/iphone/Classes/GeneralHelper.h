

#import <Foundation/Foundation.h>

@interface GeneralHelper : NSObject
+ (id)sharedManager;
- (NSString*)appstoreLink;
- (UIImage *)imageWithColor:(UIColor *)color;
@end
