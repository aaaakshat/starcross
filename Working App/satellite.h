#ifndef __STARCROSS_SATELLITE_H__
#define __STARCROSS_SATELLITE_H__

#include <string>
#include <vector>
#include "stars/straight.h"

namespace starcross {
	
	struct Satellite {
		r3::Vec3f pos;
		std::string name;
	};
    
    struct Telescope {
        stars::Vec3f pos;
        std::string name;
    };
    
    struct ConstallationsOverlay {
        stars::Vec3f pos;
        std::string name;
    };
	
	void ReadSatelliteData( const std::string & filename );
	void ComputeSatellitePositions( std::vector<Satellite> & satellites );
	
    void ReadTelescopeData( const std::string & filename );
    void ComputeTelescopePositions( std::vector<Telescope> & telescopes );
    
    void ReadConstellationsData( const std::string & filename );
    void ComputeConstellationsPositions( std::vector<ConstallationsOverlay> & constellationsOverlay );
    
    
	// Get the rotation of the earth relative to ECI
	float GetCurrentEarthPhase();
	double GetJulianDate();

}




#endif // __STARCROSS_SATELLITE_H__
