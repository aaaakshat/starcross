#ifndef __CARTESIAN_DEF__
#define __CARTESIAN_DEF__

#include "stars/straight.h"
#include <math.h>

inline r3::Vec3f xHat() {
	return stars::Vec3f(1.0, 0.0, 0.0);
}

inline stars::Vec3f yHat() {
	return stars::Vec3f(0.0, 1.0, 0.0);
}

inline stars::Vec3f zHat() {
	return stars::Vec3f(0.0, 0.0, 1.0);
}

inline float angleInPlane( const stars::Vec3f & vec , const stars::Vec3f & u, const stars::Vec3f & v ) {
	return atan2( vec.Dot(v), vec.Dot(u) );
}
 
inline stars::Vec3f latLongToUnitVector(float lat, float lon) {
	return stars::Vec3f( cos(lat)*cos(lon), cos(lat)*sin(lon), sin(lat) );
}

		
inline float angleBetween(const stars::Vec3f & u, const stars::Vec3f & v) {
	stars::Vec3f a = u / u.Length();
	stars::Vec3f b = v / v.Length();
	return acos( a.Dot(b) );
}

#endif //__CARTESIAN_DEF__
