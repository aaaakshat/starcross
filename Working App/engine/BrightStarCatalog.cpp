
//#include <eikenv.h>
//#include "f32file.h"
#include "BrightStarCatalog.h"
#include "stars/filing.h"
#include <stdio.h>


using namespace stars;
using namespace std;

bool 
BrightStarCatalog::Initialized()
{
	if ( stars.size() > 0 ) {
		return true;
	}
	
	File *file = FileOpenForRead( "stars" );
	if (file == NULL )
	{
		return false;
	}

	int len = file->Size() / (8 * 3);
	for ( int i = 0; i < len; i++ ) {
		double vals[3]; 
		file->Read( vals, 8, 3 );
		Star s;
		s.ra = float( vals[0] );
		s.dec = float( vals[1] );
		s.mag = float( vals[2] );
		stars.push_back( s );
	}
	delete file;
	return true;
}

float
BrightStarCatalog::rightAscensionInRadians(int i)
{
	return stars[ i ].ra;
}

float
BrightStarCatalog::declinationInRadians(int i)
{
	return stars[ i ].dec;
}

float
BrightStarCatalog::mag(int i)
{
	return stars[ i ].mag;
}

