#ifndef __STARS_OBJECTS_H__
#define __STARS_OBJECTS_H__

#include "stars/show.h"
#include <string>

namespace stars {
	Model * CreateModelFromObjFile( const std::string & filename );
}


#endif // __STARS_OBJECTS_H__
