#ifndef __STARS_RENDERING_H__
#define __STARS_RENDERING_H__

#include "stars/lay.h"
#include "stars/texture.h"

namespace stars {

	class RenderBuffer {
		TextureFormatEnum format;
		int width;
		int height;
		uint obj;
		RenderBuffer( TextureFormatEnum rbFormat, int rbWidth, int rbHeight );
	public:
		~RenderBuffer();
		static RenderBuffer *Create( TextureFormatEnum format, int width, int height );
		void Bind();
		void Unbind();
	};
	
}

#endif // __STARS_RENDERING_H__

