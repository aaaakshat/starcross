#ifndef __STARS_PARSING_H__
#define __STARS_PARSING_H__

#include <string>
#include <vector>

namespace stars {
	enum TokenType {
		TokenType_Number,
		TokenType_String
	};
	
	struct Token {
		Token() {}
		Token( const std::string & str ) {
			type = TokenType_String;
			valString = str;
		}
		TokenType type;
		float valNumber;       // set if type == TokenType_Number
		std::string valString; // always set
	};

	bool StringToFloat( const std::string & s, float & val );
	
	std::vector< Token > TokenizeString( const char *str, const char *delimiters = " \t" );
}

#endif // __STARS_PARSING_H__
