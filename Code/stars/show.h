#ifndef __STARS_SHOW_H__
#define __STARS_SHOW_H__

#include "stars/buffering.h"
#include "stars/drawing.h"
#include "stars/straight.h"

#include <string>

namespace stars {
	
	void InitModel();
	
	class Model {
		std::string name;
		VertexBuffer *vertexBuffer;
		IndexBuffer *indexBuffer;
		PrimitiveEnum prim;
		// disallow copying and assignment
		Model( const Model & rhs ) {}
		const Model & operator= ( const Model & rhs ) {
			return *this;
		}
	public:
		Model( const std::string & mName );
		~Model();
		const std::string GetName() const {
			return name;
		}
		VertexBuffer & GetVertexBuffer();
		IndexBuffer & GetIndexBuffer();
		PrimitiveEnum GetPrimitive() const {
			return prim;
		}
		void SetPrimitive( PrimitiveEnum mPrim ) {
			prim = mPrim;
		}
		void Draw();		
	};

		
}

#endif // __STARS_SHOW_H__
















