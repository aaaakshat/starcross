#include "stars/commanding.h"
#include "stars/outcome.h"
#include "stars/variable.h"
#include <map>

using namespace std;
using namespace stars;

namespace {
	
	// We have to do this to have static initialization of commands works.
	
	struct Commands {
		Commands() {
			// Output("Constructing Commands singleton.\n" );
		}
		map<Atom, Command *> lookup;
	};
	
	Commands * commands = NULL;
	
}

namespace stars {
	
	void InitCommand() {
		if ( commands == NULL ) {
			commands = new Commands;
		}
	}
	
	Command::Command( const char *cmdName, const char *cmdHelpText ) 
	: name( cmdName ), helpText( cmdHelpText ) {
		InitCommand();
		//Output( "Registering command %s (%d): %s - %s\n", name.Str().c_str(), name.Val(), cmdName, cmdHelpText );
		commands->lookup[ name ] = this;
	}
	
	Command::~Command() {
		commands->lookup.erase( name );
	}
	
	Command *GetCommand( const char *cmdName ) {
		Atom name = FindAtom( cmdName );
		if ( commands->lookup.count( name ) ) {
			return commands->lookup[ name ];
		}
		return NULL;
	}
	
	void ExecuteCommand( const char *cmd ) {
		OutputDebug("executing command: \"%s\"", cmd );
		vector< Token > tokens = TokenizeString( cmd );
		if ( tokens.size() == 0 ) {
			return;
		}
		
		// try command first
		Command *c = GetCommand( tokens[0].valString.c_str() );
		if ( c ) {
			c->Execute( tokens );
			return;
		}

		// then try var
		if ( FindVar( tokens[0].valString.c_str() )  ){
			string newcmd("_et ");
			newcmd += cmd;
			newcmd[0] = tokens.size() == 1 ? 'g' : 's';
			ExecuteCommand( newcmd.c_str() );
			return;
		}
		
		Output( "Command not found." );
	}
	
}

extern VarInteger f_numOpenFiles;

namespace {

	// quit command
	void Quit( const vector< Token > & tokens ) {
		ExecuteCommand( "writebindings" );
		ExecuteCommand( "writevars" );
		ExecuteCommand( "appquit" );
	}	
	CommandFunc QuitCmd( "quit", "calls exit(0)", Quit );

	// listCommands command
	void ListCommands( const vector< Token > & tokens ) {
		map< Atom, Command * > &m = commands->lookup;
		for( map< Atom, Command * >::iterator it = m.begin(); it != m.end(); ++it ) {
			Output( "%s - %s\n", it->first.Str().c_str(), it->second->HelpText().c_str() );
		}
	}
	CommandFunc ListCommandsCmd( "listcommands", "lists registered commands", ListCommands );
	
}


