#ifndef __STARS_TEXTURE_H__
#define __STARS_TEXTURE_H__

#include <string>

namespace stars {
	
	void InitTexture();
	
	enum TextureFormatEnum {
		TextureFormat_INVALID,
		TextureFormat_L,
		TextureFormat_LA,
		TextureFormat_RGB,
		TextureFormat_RGBA,
		TextureFormat_DepthComponent,
		TextureFormat_MAX
	};

	enum TextureTargetEnum {
		TextureTarget_1D,
		TextureTarget_2D,
		TextureTarget_3D,
		TextureTarget_Cube
	};
	
	enum TextureFilterEnum {
		TextureFilter_None,
		TextureFilter_Nearest,
		TextureFilter_Linear
	};

	struct SamplerParams {
		SamplerParams() 
		: magFilter( TextureFilter_Linear )
		, minFilter( TextureFilter_Linear )
		, mipFilter( TextureFilter_Linear )
		, levelBias( 0.0f )
		, levelMin( 0.0f )
		, levelMax( 128.0f )
		, anisoMax( 4.0f )
		{}
		
		SamplerParams( TextureFilterEnum tfMagFilter, TextureFilterEnum tfMinFilter, TextureFilterEnum tfMipFilter, float tfLevelBias, float tfLevelMin, float tfLevelMax, float tfAnisoMax )
		: magFilter( tfMagFilter )
		, minFilter( tfMinFilter )
		, mipFilter( tfMipFilter )
		, levelBias( tfLevelBias )
		, levelMin( tfLevelMin )
		, levelMax( tfLevelMax )
		, anisoMax( tfAnisoMax )
		{}
		
		TextureFilterEnum magFilter;
		TextureFilterEnum minFilter;
		TextureFilterEnum mipFilter;
		float levelBias;
		float levelMin;
		float levelMax;
		float anisoMax;
	};
	
	class Texture {
		std::string name; 
		TextureFormatEnum format;
		TextureTargetEnum target;
		SamplerParams sampler;
	protected:
		Texture( const std::string & texName, TextureFormatEnum texFormat, TextureTargetEnum texTarget );
	public:
		~Texture();
		
		void Bind( int imageUnit );
		void Enable();
		void Disable();
		
		const std::string & Name() {
			return name;
		}

		TextureFormatEnum Format() {
			return format;
		}
		TextureTargetEnum Target() {
			return target;
		}
		
		const SamplerParams & Sampler() const {
			return sampler;
		}
		void SetSampler( const SamplerParams & s );
		
	};
	
	class Texture2D : public Texture {
		int width;
		int height;
		Texture2D( const std::string & n, TextureFormatEnum f, int w, int h );
	public:
		static Texture2D *Create( const std::string &n, TextureFormatEnum f, int w, int h );
		int Width() const {
			return width;
		}
		
		int Height() const {
			return height;
		}
		
		void SetImage( int level, void *data );
	};
	
	Texture2D * CreateTexture2DFromFile( const std::string & filename, TextureFormatEnum f = TextureFormat_INVALID );

	
}

#endif // __STARS_TEXTURE_H__
