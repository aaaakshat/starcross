#ifndef __STARS_FONT_H__
#define __STARS_FONT_H__

#include "stars/texture.h"
#include "stars/binding.h"

namespace stars {
	void InitFont();
	
	struct Font {
		virtual void Print( const std::string &text, float x, float y, float scale = 1.0f ) = 0;	
		virtual Bounds2f GetStringDimensions( const std::string &text, float scale = 1.0f ) = 0;
	};

	Font * CreateStbFont( const std::string & fontName, float pointSize );
	
}

#endif // __STARS_FONT_H__
