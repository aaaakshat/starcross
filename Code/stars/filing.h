#ifndef __STARS_FILING_H__
#define __STARS_FILING_H__

#include "stars/lay.h"
#include <string>
#include <vector>

namespace stars {

	void InitFilesystem();
	
	enum SeekEnum {
		Seek_Begin,
		Seek_Curr,
		Seek_End
	};

	// will need to convert many of these ints to 64 bit types at some point
	class File {
	public:
		virtual ~File() {}
		virtual int Read( void *data, int size, int nitems ) = 0;
		virtual int Write( const void *data, int size, int nitems ) = 0;
		virtual void Seek( SeekEnum whence, int offset ) = 0;
		virtual int Tell() = 0;
		virtual int Size() = 0;
		virtual bool AtEnd() = 0; 

		// convenience
		std::string ReadLine();
		void WriteLine( const std::string & str );
	};

	File * FileOpenForWrite( const std::string & filename );
	File * FileOpenForRead( const std::string & filename );
	
	bool FileReadToMemory( const std::string & filename, std::vector< uchar > & data );
	
}

#endif // __STARS_FILING_H__
