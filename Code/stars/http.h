

#ifndef __STARS_HTTP_H__
#define __STARS_HTTP_H__

#include "stars/lay.h"
#include <string>
#include <vector>

namespace stars {

	bool UrlReadToMemory( const std::string & url, std::vector< uchar > & data );

}

#endif // __STARS_HTTP_H__

